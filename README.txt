This is the binary release of the main FCAPS components maintained by CSE along with required libraries. 
The software has been based on Phase-I SELFNET project outcomes "APP Manager (https://github.com/Selfnet-5G/SDNO-APP-Mgr)" and "TAL Engine (https://github.com/Selfnet-5G/TAL-Engines)".
Additions have been applied to cater for the support of monitoring automation (FCAPS Manager) and alternative rule processing workflows (TAL Engine)
 
The binaries are deployed as Karaf blueprints.
Before, however, hot deploying it by copying the binaries into the dedicated ({karaf home}/deploy) folder a number of features and other bundles on which the implementation is based must be first installed and/or activated on a running Karaf instance through the Karaf console: 
An environment variable - JAVA_HOME - should be defined to indicate the installation of a JDK-1.8 installation.
Preparation of Karaf (apache-karaf-4.2.2):

repo-add https://repo1.maven.org/maven2/org/apache/cxf/karaf/apache-cxf/3.2.8/apache-cxf-3.2.8-features.xml
feature:install cxf-rs-description-openapi-v3  cxf-rs-description-swagger2  blueprint-web cxf  cxf-http-jetty war
bundle:install wrap:http://central.maven.org/maven2/javax/persistence/persistence-api/1.0.2/persistence-api-1.0.2.jar
bundle:install wrap:http://central.maven.org/maven2/org/hibernate/hibernate-annotations/3.5.6-Final/hibernate-annotations-3.5.6-Final.jar
bundle:install http://central.maven.org/maven2/org/mongodb/morphia/morphia/1.3.2/morphia-1.3.2.jar
bundle:install http://central.maven.org/maven2/org/mongodb/mongo-java-driver/3.9.1/mongo-java-driver-3.9.1.jar
bundle:install http://central.maven.org/maven2/com/rabbitmq/amqp-client/5.6.0/amqp-client-5.6.0.jar
bundle:install wrap:http://central.maven.org/maven2/com/spotify/docker-client/8.15.0/docker-client-8.15.0.jar
bundle:install http://central.maven.org/maven2/org/apache/commons/commons-compress/1.18/commons-compress-1.18.jar
bundle:install http://central.maven.org/maven2/commons-lang/commons-lang/2.6/commons-lang-2.6.jar
bundle:install wrap:file:///home/kkoutso/dev/slicenet/dev/WP6/fcapsManagerLibs/httpclient-4.5.7.jar
bundle:install wrap:file:///home/kkoutso/dev/slicenet/dev/WP6/fcapsManagerLibs/httpcore-4.4.9.jar
bundle:install http://central.maven.org/maven2/mysql/mysql-connector-java/8.0.15/mysql-connector-java-8.0.15.jar
bundle:install file:///{...path to ...}/fcapsManagerLibs/accessors-smart-1.2.jar
bundle:install file:///{...path to ...}/fcapsManagerLibs/asm-5.0.4.jar
bundle:install file:///{...path to ...}/fcapsManagerLibs/commons-io-1.3.2.jar
bundle:install file:///{...path to ...}/fcapsManagerLibs/jsch-0.1.54.jar
bundle:install file:///{...path to ...}/fcapsManagerLibs/json-path-2.4.0.jar
bundle:install file:///{...path to ...}/fcapsManagerLibs/json-smart-2.3.jar
bundle:install file:///{...path to ...}/fcapsManagerLibs/onboarding-1.0-SNAPSHOT.jar
bundle:install file:///{...path to ...}/fcapsManagerLibs/proxytoys-0.2.jar
bundle:install file:///{...path to ...}/fcapsManagerLibs/json-path-2.4.0.jar
bundle:install file:///{...path to ...}/fcapsManagerLibs/json-smart-2.3.jar
bundle:install file:///{...path to ...}/fcapsManagerLibs/onboarding-1.0-SNAPSHOT.jar
bundle:install file:///{...path to ...}/fcapsManagerLibs/proxytoys-0.2.jar
bunlde:install file:///{...path to ...}/fcapsManagerLibs/docker-client-8.1.2-jar-with-dependencies.jar
bundle:install wrap:file:///{...path to ...}/fcapsManagerLibs/xercesImpl-2.12.0.jar
bundle:install http://central.maven.org/maven2/com/rabbitmq/amqp-client/4.9.3/amqp-client-4.9.3.jar
bundle:install wrap:file:///{...path to ...}/fcapsManagerLibs/kafka-clients-2.2.1.jar

FCAPS Manager persists information on a MongoDB document database. Therefore a MongoDB daemon should be launched, e.g.:
mongod  --dbpath sdno_mongo
A Docker daemon should be also up and running for the management of the docker images and containers (a dedicated folder is used for the images and containers metadata):
dockerd -d ./data
FCAPS Manager is using two specific docker images that are activated for telegraf and custom artifacts respectively. 
For the preparation of suchs images a temporary folder should be created and inside it a file with name Dokcerfile must be present. The contents should be:

for the custom artifact 
FROM ubuntu:latest
RUN apt-get -y update
RUN apt-get install -y python3
RUN apt-get install -y python3-requests
RUN apt-get install -y net-tools
RUN apt-get install -y iputils-ping
ADD jre1.8.0_202.tar.gz  /opt/
ENV PATH=$PATH:/opt/jre1.8.0_202/bin
ENV JAVA_HOME=/opt/jre1.8.0_202/
CMD /bin/bash
Java is installed by extracting the indicated jre as obtained from Oracle distribution server. To instruct docker create the image the following command should be used:
docker build -t sdnapp .
“sdnapp" is the name of the image to be created.

for the custom artifact the previous image is resused and expanded with addition of the telegraf binary
FROM sdnapp
ADD telegraf /opt/telegraf

docker build -t telegraf .
“telegraf" is the name of the image to be created.



FCAPS Manager is further configured by a file SDNO.cfg located in /opt/{apache karaf installation/etc/ with the following contents:

SDN_CONTROLLER_TYPE = ODL
SDN_CONTROLLER_IP = localhost
SDN_CONTROLLER_USERNAME = karaf
SDN_CONTROLLER_PASSWORD = karaf
CATALOGUE_IP = 127.0.0.1
CATALOGUE_USERNAME = selfnet
CATALOGUE_PASSWORD = selfnet
APPMGR_IP = 10.4.1.22
VNFM_IP = 10.4.1.32
CATALOGUE_IF=false
INVENTORY_MONGO=127.0.0.1
INVENTORY_MYSQL=127.0.0.1
INVENTORY_MYSQL_USER=admin
INVENTORY_MYSQL_PWD=admin

Also for TAL Engine there is need for a similar TALCORE.cfg: 

TAL_ENGINE_WP4 = 127.0.0.1
TAL_ENGINE_WP5 = 127.0.0.1
KAFKA_BUS      = 127.0.0.1
KAFKA_TOPIC    = alarms_predictions
REPUBLISH_TACTICS    = false
PUBLISH_TACTICS    = true
CACHE_TACTICS    = false
SENT_DIRECTLY_TO_SO    = false
AE_CALLBACK = http://127.0.0.1:18183/tal/engine/callback
SO_IP = http://127.0.0.1:9004
SO_PATH = /ptinovacao/serviceOrderingManagement/serviceOrder/
CPSR=http://127.0.0.1:8080/slicenet/ctrlplane/cpsr_disc/v1/cps-instances?cpsType=



